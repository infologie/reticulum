---
title: Reticulum 1 – Opérabilité du concept de dispositif
color: green
---

{{< image src="/img/reticulum1-affiche.jpg" alt="Crédit photo : Paul Feudal https://paulfudal.com/teufelsberg/" position="center" style="border-radius: 8px;" >}}

C’est un mot qui peut se traduire par intersection, maillage ou réseau ; mais il peut également signifier un filet, voire un piège. Reticulum : une locution appropriée pour caractériser ce paradigme multiforme que notre époque a vu se renforcer – celui des réseaux. Un terme polysémique, qui souligne la nécessité d’un travail épistémologique partagé. C’est dans cet esprit que nous avons pensé ce cycle de rencontres entre SIC et design, dont la première édition s’intitule **Opérabilité du concept de dispositif**.

# Synopsis de la journée

Le dispositif a connu de nombreux soubresauts épistémologiques depuis sa mise en avant dans la pensée foucaldienne, notamment à travers sa réappropriation dans le langage des SIC et son actualisation à une époque d’inquiétude sécuritaire et de renouveau des luttes sociales. Un énoncé de Foucault extrait de *Dits et Écrits vol. III* finit souvent par faire office de définition dans les travaux de recherche : le dispositif y est décrit comme « un ensemble résolument hétérogène » ou plus précisément comme « le réseau qu'on peut établir entre ces éléments » et qui possède une « fonction stratégique dominante » (Foucault, 1977). Giorgio Agamben, trente ans plus tard, le caractérise par ses finalités : « tout ce qui a, d’une manière ou une autre, la capacité de capturer, d’orienter, de déterminer, d’intercepter, de modeler, de contrôler et d’assurer les gestes, les conduites, les opinions et les discours des êtres vivants » (Agamben, 2007). Le dispositif fait partie des concepts clefs des SIC ; à ce titre il fait l’objet d’un travail régulier de réactualisation et de critique (Jacquinot-Delaunay & Monnoyer, 1999 ; Appel, Boulanger, & Massou, 2010 ; Larroche, 2018).

Au-delà d’un tracé historique ou d’une analyse pharmacologique du concept, l’ambition de cette journée est d’interroger des chercheurs et chercheuses issus de disciplines différentes quant à l’opérabilité du dispositif dans leurs travaux. Relecture, actualisation, abandon, confrontation ou encore dépassement, les postures face au dispositif dans la recherche en SIC et en design sont extrêmement diverses mais souvent pertinentes. Le temps de conférences, tables rondes et présentations de projets, cette première édition de Reticulum sera l’opportunité d’opérer collectivement une réflexion épistémologique sur ce concept majeur que constitue le dispositif.

## Références

- Agamben, G. (2007). *Qu’est-ce qu’un dispositif ?* (M. Rueff, Trad.). Paris : Éditions Payot & Rivages.
- Appel, V., Boulanger, H., & Massou, L. (Éd.). (2010). *Les dispositifs d’information et de communication : concept, usages et objets.* Bruxelles : De Boeck.
- Foucault, M. (1977). « Le jeu de Michel Foucault ». In D. Defert & F. Ewald (Éd.), *Dits et écrits, 1954-1988. III, 1976-1979.* Paris : Gallimard.
- Jacquinot-Delaunay, G., & Monnoyer, L. (Éd.). (1999). *Le dispositif : entre usage et concept.* Paris : CNRS Éditions.
- Larroche, V. (2018). *Le dispositif : un concept pour les sciences de l’information et de la communication.* Londres : ISTE éditions.

# Programmation

| 09h00-09h30 | Accueil des participants |
|-|-|
| 09h30-10h15 | **Introduction de la journée d’étude** Florian Harmand & Arthur Perret (UBM, MICA) |
| 10h15-11h00 | **Comprendre le dispositif par les activités techniques distribuées et le discours** Valérie Larroche (ENSSIB, Elico) |
| 11h00-11h15 | Pause |
| 11h15-12h00 | **Dispositif et opérationnalité de la connaissance : une introduction aux études digitales** Franck Cormerais (UBM, Mica) |
| 12h00-12h45 | **Discussions croisées : peut-on encore parler de dispositif(s) en SIC ?** Valérie Larroche (ENSSIB, Elico), Franck Cormerais (UBM, Mica) |
| 12h45-14h00 | Repas (buffet ouvert) |
| 14h00-15h00 | **Présentation de projet** Hélène Sauzéon (Inria, Flowers) |
| 15h00-15h15 | Pause |
| 15h15-16h15 | **Présentations de projets** Camille Forthoffer (UBM, Mica) Arthur Perret & Lucie Vieillecroze (UBM, Mica) |
| 16h15-16h30 | Pause |
| 16h30-17h30 | **Présentations de projets** Julien Gachadoat (studio 2roqs) Emna Kamoun (UBM, MICA) |
| 17h30 | Clôture |

[Télécharger le programme (PDF)](/reticulum1-programme.pdf)

# Informations pratiques

Le lundi 04 mars 2019 de 09h30 à 18h00, à la MSHA de l'Université Bordeaux Montaigne, Domaine Universitaire, 10 Esplanade des Antilles 33607 PESSAC. [Voir sur Google Maps](https://www.google.com/maps/place/Maison+Sciences+Homme+Aquitaine/@44.7944306,-0.6187195,15z/data=!4m5!3m4!1s0x0:0xca5004bfedc5fc87!8m2!3d44.7944306!4d-0.6187195)

