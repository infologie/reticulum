---
title: Reticulum 3
color: blue
---

{{< image src="/img/reticulum3-pessac.png" alt="Données OpenStreetMap. Génération : Andrei Kashcha anvaka.github.io/city-roads/" position="center" style="border-radius: 8px;" >}}

# Synopsis de la journée

Initié en 2018, le cycle de rencontres *Reticulum* explore les transformations de la recherche et de la production d’artefacts par le paradigme des réseaux, à la croisée des sciences de l’information et de la communication (SIC) et du design. La première édition (2019) portait sur l'opérabilité du concept de dispositif, son rôle heuristique et sa pertinence dans une approche holistique du projet de design. Elle a mis en valeur de nouvelles lectures et approches de recherche. La seconde édition (2020) concernait l’architecture de l’information. Les contributions ont mis en lumière le caractère pertinent et actuel de cette notion pour contrecarrer l'infobésité, parer au déluge de données et préserver l'intelligibilité partagée de notre milieu.

Au fil des échanges, ces deux premières éditions ont fait ressortir le rôle déterminant que jouent les outils dans le perfectionnement de nos capacités d’exploration et d’enquête, ainsi que l’intelligibilité et la communicabilité de nos productions. C’est pourquoi la troisième édition de *Reticulum* sera consacrée aux techniques de la réticularisation numérique et à leurs apports. Structuration des données, mise en relation des matériaux de recherche, modularité des systèmes, production simultanée de formats multiples, collaboration et automatisation : la logique réticulaire ouvre un certain nombre de potentialités. Pour les actualiser, tirer parti de leur capacité nodale et nexiale, il faut les implémenter dans nos processus de conception, de production, de documentation et d’éditorialisation. L’enjeu n’est autre que l’imbrication du faire et du savoir, afin que la théorie progresse par la pratique et vice versa.

Cette thématique s’inscrit dans la continuité du cycle, qui depuis ses débuts met en lumière les projets de recherche et leurs livrables. La convergence des savoirs et des savoir-faire, des métiers et de leur enseignement (Jeanneret et Ollivier, 2004), advient essentiellement dans cette activité, ce déroulement du projet comme espace – et comme temps – de « l’exploration, la manipulation, l’organisation » propices à l’invention (Simondon, 2008, 151). Plus encore, l’appropriation des outils – voire leur construction *ad hoc* – nous apparaît comme condition *sine qua non* du développement d’une « culture technique » (Simondon, 2012) dans le milieu de la recherche. Comme l’énonçait Foucault, il faut peut-être cesser de croire que « la renonciation au pouvoir est une des conditions auxquelles on peut devenir savant » (1993, 36), et envisager une recherche affranchie de certaines contraintes industrielles (systèmes exogènes fermés, hégémonie dans les formats) ou encore organisationnelles (gestion centralisée des services informatiques).

Dans cette perspective, Reticulum 3 se déroulera cette année en deux temps :

- L’atelier (décembre 2020 ou janvier 2021). En amont de la journée, les étudiants en design (Master DIIS) et en humanités numériques (Master DNHD) seront réunis au sein d’un atelier constituant une formation par la recherche aux techniques évoquées plus haut. Deux jours seront dédiés à une approche réflexive du projet de design, avec production de contenus (études de cas, retours d’expérience) ; deux autres jours seront dédiés à leur architecture et à leur publication. Une chaîne d’édition hybride permettra la production d’un double livrable, numérique et imprimé. Une mise à niveau technique et conceptuelle préalable sera proposée aux étudiants : chaîne de publication et design systémique (notions), utilisation d’un éditeur de texte, HTML/CSS et GitLab (outils).
- La journée d’étude (février 2021). Dans la lignée des deux premières éditions, la journée d’étude alternera des propositions de chercheurs et des présentations de professionnels, qui illustreront et discuteront ce que peut être une recherche outillée par des techniques numériques et réticulaires. Deux sous-thématiques se répondront plus particulièrement : l’une sera orientée vers les processus de conception/production, dans une perspective de design ; l’autre portera sur les possibilités d’architecture et d’éditorialisation suivant une approche documentaire.

## Références

- Foucault, Michel. (1975) 1993. Surveiller et punir : naissance de la prison. Gallimard.
- Jeanneret, Yves, et Ollivier, Bruno, éd. 2004. Les sciences de l’information et de la communication, savoirs et pouvoirs. Hermès, 38. CNRS éditions. <http://www.cairn.info/revue-hermes-la-revue-2004-1.htm>
- Simondon, Gilbert. 2008. Imagination et invention (1965-1966). Éditions de la Transparence.
- Simondon, Gilbert. 2012. Du mode d’existence des objets techniques. Aubier.

# Programmation

À venir.

<!-- 
| 09h30-10h00 | Accueil des participants |
|-|-|
| 10h00-10h30 | **\[Intro\] Architecture de l’information : design & SIC** Florian Harmand (UBM, MICA) |
| 10h30-11h00 | **\[Intro\] Quelques liens entre documentation, sémiotique et architecture de l'information** Arthur Perret (UBM, Mica) |
| 11h00-11h45 | **Architecture de l’information vs. design d’information** Anne Beyaert-Geslin (UBM, Mica) |
| 11h45-12h30 | **De l’architecture à l’ontologie** Karl Pineau (UTC, Costech) |
| 12h30-14h00 | Repas (buffet ouvert) |
| 14h00-15h00 | **AI et attention** Noémie Antoine (Indépendante) **AI et objets intermédiaires** Yelen Atchou (Inflexsys) |
| 15h00-16h00 | **L'architecte et l'architecte de l'information** Alice Totaro, Sami Lini (Akiani) **AI et conception web** Baptiste Prébot (ENSC) |
| 16h00-16h45 | Discussions croisées |
| 16h45-17h00 | Restitutions de travaux du Master Design |
| 17h00-17h30 | Clôture de la journée |
 -->

<!-- [Télécharger le programme (PDF)](/reticulum2-programme.pdf) -->

# Informations pratiques

À venir.

<!-- Le jeudi 20 février 2020 de 09h30 à 17h30, à l'Université Bordeaux Montaigne, Maison des Sciences de l'Homme d'Aquitaine Salle Jean Borde (rdc à gauche) 10 Esplanade des Antilles 33607 PESSAC. [Voir sur Google Maps](https://www.google.com/maps/place/Maison+Sciences+Homme+Aquitaine/@44.7944306,-0.6187195,15z/data=!4m5!3m4!1s0x0:0xca5004bfedc5fc87!8m2!3d44.7944306!4d-0.6187195) -->