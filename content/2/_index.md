---
title: Reticulum 2 – Architecture(s) de l'information
color: orange
---

{{< image src="/img/reticulum2-affiche.jpg" alt="Crédit photo : Julien Gachadoat et Florian Harmand" position="center" style="border-radius: 8px;" >}}

La naissance de l'architecture de l'information en tant que champ est concomitante de l'ère de l'information engendrée par les techniques numériques. En d'autres mots, elle fait partie de ces approches permettant de lire et d'écrire le réseau, mais aussi de le rendre accessible et intelligible au plus grand nombre. C'est pourquoi l'architecture de l'information est au cœur de la deuxième édition de Reticulum, un cycle de rencontres interdisciplinaires entre SIC et design.

# Synopsis de la journée

Après une première édition consacrée à l’opérabilité du concept de dispositif, les rencontres Reticulum souhaitent aborder une autre notion à l’intersection des SIC et du design : l’architecture de l’information. Si cette locution agrège deux termes complexes, on en comprend rapidement l’enjeu : il s’agit de maîtriser (ἀρχός / arkhós : celui qui gouverne) la production (τίκτω / tíktô : produire) des informations. A l’instar du « premier art » dont elle emprunte le nom, l’architecture de l’information est liée aux structures qui organisent les existences humaines : elle opère dans l’espace et le temps, conditionnant des flux virtuels, sève de la « société de l’information » (Bell 1979, Mattelart 2000).

L’expression « architecture de l’information » a été proposée par Richard Wurman en 1976. Comme pour la documentation au début du 20e siècle, elle s’est construite comme une réponse à la croissance informationnelle de son temps. Visant avant tout l’opérabilité — *to make something work* —, elle s’est essentiellement développée comme un champ de pratiques professionnelles, tout en cultivant une certaine réflexion théorique. Au tournant des années 2000, son positionnement est devenu fort complexe sous la pression d’une pensée plus forte du design. En effet, elle partage avec le design d’information les « relations ‫à l’espace, ‪aux signes et au sens » (Beyaert-Geslin, 2018) ; de plus, la logique d’organisation des contenus qu’elle met en œuvre se confond avec celle d’autres champs, comme l’éditorialisation (Lipsyc & Ihadjadene, 2013 ; Zacklad, 2019).

La présence de la métaphore architecturale en design et dans les différentes sciences de l’information interroge sur le périmètre et la pertinence de l’architecture de l’information. C’est dans cette perspective que nous proposons une deuxième journée Reticulum intitulée **Architecture(s) de l’information**, afin de la questionner à la fois en tant qu’objet et en tant que champ. Elle réunira chercheurs, étudiants et professionnels pour explorer les subtilités, potentiels et limites d’un domaine au carrefour de champs scientifiques et de pratiques de conception.


## Références

- Beyaert-Geslin, A. (2018). ‪Architecture de l’information versus design de l’information‪. *Etudes de communication*, n° 50(1), 161-174. DOI: [10.4000/edc.7651](https://doi.org/10.4000/edc.7651)
- Lipsyc, C., & Ihadjadene, M. (2013). Architecture de l’information et éditorialisation. *Études de communication*, (41), 103-118. DOI: [10.4000/edc.5406](https://doi.org/10.4000/edc.5406)
- Zacklad, M. (2019). Le design de l’information : textualisation, documentarisation, auctorialisation. *Communication & langages*, n° 199, 37-64. DOI: [10.3917/comla1.199.0037](https://doi.org/10.3917/comla1.199.0037)

# Programmation

| 09h30-10h00 | Accueil des participants |
|-|-|
| 10h00-10h30 | **\[Intro\] Architecture de l’information : design & SIC** Florian Harmand (UBM, MICA) |
| 10h30-11h00 | **\[Intro\] Quelques liens entre documentation, sémiotique et architecture de l'information** Arthur Perret (UBM, Mica) |
| 11h00-11h45 | **Architecture de l’information vs. design d’information** Anne Beyaert-Geslin (UBM, Mica) |
| 11h45-12h30 | **De l’architecture à l’ontologie** Karl Pineau (UTC, Costech) |
| 12h30-14h00 | Repas (buffet ouvert) |
| 14h00-15h00 | **AI et attention** Noémie Antoine (Indépendante) **AI et objets intermédiaires** Yelen Atchou (Inflexsys) |
| 15h00-16h00 | **L'architecte et l'architecte de l'information** Alice Totaro, Sami Lini (Akiani) **AI et conception web** Baptiste Prébot (ENSC) |
| 16h00-16h45 | Discussions croisées |
| 16h45-17h00 | Restitutions de travaux du Master Design |
| 17h00-17h30 | Clôture de la journée |

[Télécharger le programme (PDF)](/reticulum2-programme.pdf)

# Informations pratiques

Le jeudi 20 février 2020 de 09h30 à 17h30, à l'Université Bordeaux Montaigne, Maison des Sciences de l'Homme d'Aquitaine Salle Jean Borde (rdc à gauche) 10 Esplanade des Antilles 33607 PESSAC. [Voir sur Google Maps](https://www.google.com/maps/place/Maison+Sciences+Homme+Aquitaine/@44.7944306,-0.6187195,15z/data=!4m5!3m4!1s0x0:0xca5004bfedc5fc87!8m2!3d44.7944306!4d-0.6187195)