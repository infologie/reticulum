---
title: Reticulum
---

Co-organisées par Florian Harmand et Arthur Perret (Université Bordeaux Montaigne, laboratoire Mica), les rencontres Reticulum offrent un lieu et un temps pour interroger le paradigme réticulaire, plus que jamais prégnant à l’ère numérique. Réunissant des chercheurs et des praticiens, ces événements mettent en rapport des travaux de recherches et des projets professionnels ayant un lien intime avec le réseau et ses concepts afférents.

Cette démarche se veut épistémologique, au sens où elle questionne la genèse et l’évolution de concepts cruciaux des SIC et du design à l’interface de la recherche et des pratiques professionnelles.

Il nous importe également que les journées Reticulum restent accessibles aux étudiants de Master, aux doctorants et aux chercheurs de toutes disciplines, c’est pourquoi nous travaillons avec les intervenants de manière à proposer un contenu vulgarisé tout en conservant de larges plages d’échanges.

Reticulum a reçu le soutien de l'école doctorale Montaigne Humanités, du laboratoire MICA et du programme ANR HyperOtlet.